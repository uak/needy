from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel


class HomePage(Page):
    """Home page class with title and subtitle and body"""
    # Don't allow more than one page
    max_count = 1
    subtitle = models.CharField(max_length=150, blank=False, null=True)
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('subtitle', classname="full"),
        FieldPanel('body', classname="full"),
    ]

class StaticPage(Page):
    """Static pages for the website"""
    template = 'home/home_page.html'
    subtitle = models.CharField(max_length=150, blank=False, null=True)
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('subtitle', classname="full"),
        FieldPanel('body', classname="full"),
    ]
    
