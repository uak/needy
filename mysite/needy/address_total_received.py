import base64   
import urllib.request, json
from urllib.request import Request     

#bitsocket_url = 'https://slpdb.fountainhead.cash/q/'
bitsocket_url = 'https://slpdb.bitcoin.com/q/'

# ~ tokenIdHex = "4de69e374a8ed21cbddd47f2338cc0f479dc58daa2bbe11cd604ca488eca0ddf"
# ~ address = "simpleledger:qzat73q7rjxh435ugl05gxrfghqwn5hurg36yyxhlc"

user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
headers={'User-Agent':user_agent,} 

# ~ data["q"]["aggregate"][0]["$match"]["tokenDetails.tokenIdHex"] = 10
# ~ data["q"]["aggregate"][0]["$match"]["graphTxn.outputs"]["$elemMatch"]["address"]


def prepare_query(tokenIdHex, address):
    data = {
        "v": 3,
        "q": {
            "db": ["g"],
            "aggregate": [
                {
                    "$match": {
                        "tokenDetails.tokenIdHex": tokenIdHex,
                        "graphTxn.outputs": {
                            "$elemMatch": {"address": address, "slpAmount": {"$gt": 0}}
                        },
                    }
                },
                {"$unwind": "$graphTxn.outputs"},
                {
                    "$match": {
                        "graphTxn.inputs.address": {"$ne": address},
                        "graphTxn.outputs.address": address,
                        "graphTxn.outputs.slpAmount": {"$gt": 0},
                    }
                },
                {
                    "$project": {
                        "amount": "$graphTxn.outputs.slpAmount",
                        "address": "$graphTxn.outputs.address",
                        "txid": "$graphTxn.txid",
                        "vout": "$graphTxn.outputs.vout",
                        "tokenId": "$tokenDetails.tokenIdHex",
                    }
                },
                {
                    "$group": {
                        "_id": "$tokenId",
                        "amount": {"$sum": "$amount"},
                        "address": {"$first": "$address"},
                    }
                },
            ],
        },
        "r": {"f": "[.[] | {value: .amount}]"},
    }

    return(data)


## SEtup these values 


def query_bitsocket(query):
  # convert query from python format to json 
  json_string = bytes(json.dumps(query), 'utf-8')
  url = bitsocket_url + base64.b64encode(json_string).decode('utf-8')
  #print(url)
  req = urllib.request.Request(
    url, 
    data=None, 
    headers= headers
  )
  #print(url)
  response = urllib.request.urlopen(Request(url, headers=headers))
  j = json.loads(response.read())
  #print(response)
  # ~ print(j["g"][0]["value"])
  # ~ print("value 0", j["g"][0])
  total_value = j["g"][0]["value"]
  return total_value


def get_token_total(tokenIdHex, address):
    global total_value
    data = prepare_query(tokenIdHex, address)
    return query_bitsocket(data)
