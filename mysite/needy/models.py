from django.db import models
from django import forms
from django.utils.text import slugify
from django.utils.timezone import now

# Used to Generate random user address
import random

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.snippets.models import register_snippet
from wagtail.documents.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalManyToManyField

# Random string for Needy internal ID, should be replaced by better
# method later
def random_string():
    return str(random.randint(10000, 99999))


# The index page which needy pages are its child
class NeedyIndexPage(Page):
    """ Lists needy people"""
    # Restrict to one page
    max_count = 1
    intro = RichTextField(blank=True)

    def get_context(self, request):
        # Update context to include only published posts, ordered by reverse-chron
        context = super().get_context(request)
        needypages = self.get_children().live().order_by('-first_published_at')
        context['needypages'] = needypages
        return context

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]


class NeedyPage(Page):
    """Needy page model that shows the needy information"""
    template = "needy/needy_page.html"
    # Prevent creating child pages
    subpage_types = []
    # Variable to store the random string without effecting database
    needy_serial_number = random_string
    # Id used in slugs and to have needy's website specific ID
    needy_serial = models.CharField(max_length=5, unique=True, verbose_name='needy ID',
                                help_text="field to use for slugs.", default=needy_serial_number
                                )
    name = models.CharField(max_length=25, blank=False, null=True, verbose_name='name')
    father_name = models.CharField(max_length=25, blank=False, null=True, verbose_name='father name')
    grandfather_name = models.CharField(max_length=25, blank=False, null=True, verbose_name='grand father')
    great_grandfather = models.CharField(max_length=25, blank=False, null=True, verbose_name='great grandfather')
    birth_date = models.DateField("Birth Date", blank=False, null=True)
    # Birth date in different calenders other than Gregorian
    # ex: Chinese, Arabic or Persian. Automated switcher might be used.
    birth_date_local = models.DateField("Custom Birth Date", blank=False, null=True)
    needy_contact_info = models.CharField(max_length=100, blank=False, null=True, verbose_name='your contact info')
    # Official issued ID
    id_number = models.CharField(max_length=15, blank=False, null=True, verbose_name='ID')
    id_type = models.CharField(max_length=15, blank=False, null=True, verbose_name='ID Type')
    current_street_home = models.CharField(max_length=100, blank=False, null=True, verbose_name='Street')
    profession = models.CharField(max_length=50, blank=False, null=True, verbose_name='profession')
    # In case different from profession
    current_business = models.CharField(max_length=50, blank=False, null=True, verbose_name='current work')
    income = models.PositiveSmallIntegerField(blank=False, null=True, verbose_name='monthly income')
    residence_type = models.ForeignKey('needy.ResidenceType',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name='residence type',
        related_name='+'
    )
    special_situation = ParentalManyToManyField('needy.SpecialSituation', blank=True, verbose_name='special situation')
    # Multiple choice example, dressed_in = CharField(choices=[('red','red'),('green','green'),('yellow','yellow')])
    dependent_count = models.PositiveSmallIntegerField(blank=False, null=True, verbose_name='dependent count')
    # List of dependents name and relation
    dependent_list = models.CharField(max_length=100, blank=False, null=True, verbose_name='dependent list')
    rent_expenses = models.PositiveSmallIntegerField(blank=False, null=True, verbose_name='rent expenses')
    charity_near = models.ForeignKey('needy.Charity',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    receives_charity = models.BooleanField(blank=True, null=True,
                                        verbose_name='supported by charity')
    # Name of other charity that they depends on
    other_charity = models.CharField(max_length=100, blank=True, null=True, verbose_name='other charity name')
    # Field for tests
    temp = models.CharField(max_length=100, blank=False, null=True, verbose_name='temp')
    # Needy crypto wallet address
    wallet_address = models.CharField(max_length=55, blank=False, null=True, verbose_name='wallet address')
    previous_wallet_address = models.CharField(max_length=55, blank=True, null=True, verbose_name='previous wallet address')
    single_use_wallet = models.BooleanField(blank=True, null=True, verbose_name='single use wallet')
    # Needy can explain there issue here in written format
    issue = RichTextField(features=["bold", "italic"], blank=False, null=True, verbose_name='issue')
    # Audio files for needy recording and confirming some of his information
    audio_file = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    # Needy official ID photo
    id_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name='ID photo',
        # using the same field name"
        related_name="+"
    )
    # Image for the building or house door to easily reach the needy
    door_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name='residence photo',
        related_name="+"
    )

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel("name"),
            FieldPanel("father_name"),
            FieldPanel("grandfather_name"),
            FieldPanel("great_grandfather"),
            FieldPanel("birth_date"),
            FieldPanel("birth_date_local"),
            FieldPanel("needy_contact_info"),
            FieldPanel("id_number"),
            FieldPanel("id_type"),
            FieldPanel("current_street_home"),
            FieldPanel("profession"),
            FieldPanel("current_business"),
            FieldPanel("income"),
            FieldPanel("dependent_count"),
            FieldPanel("dependent_list"),
            FieldPanel("rent_expenses"),
            FieldPanel("receives_charity"),
            FieldPanel("other_charity"),
            FieldPanel("temp"),
            FieldPanel("issue"),
            # Should be replaced with audio file upload
            DocumentChooserPanel('audio_file'),
            FieldPanel("wallet_address"),
            FieldPanel("single_use_wallet"),
            FieldPanel("special_situation", widget=forms.CheckboxSelectMultiple),
            FieldPanel("residence_type", widget=forms.RadioSelect),
            FieldPanel("charity_near"),
            ImageChooserPanel("id_image"),
            ImageChooserPanel("door_image"),
        ], heading="Address Options"),
    ]

    def clean(self):
        """Override the values of title and slug before saving."""
        # super(MatchPage, self).clean() # Python 2.X syntax
        super().clean()
        # Using needy_serial as a slug for the page
        numbered_slug = self.needy_serial
        # Use name + father name as title
        # self.title = str(name +" " + father_name)
        self.slug = slugify(numbered_slug)  # slug MUST be unique & slug-format

    class Meta:
        verbose_name = "Needy Page"
        verbose_name_plural = "Needy Pages"

@register_snippet
class Comment(models.Model):
    """ Class for storing comments by moderators"""
    needy_id_number = models.ForeignKey('needy.NeedyPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    needy_review = models.CharField(max_length=100, blank=False, null=True, verbose_name='Comments')
    def __str__(self):
        return self.needy_review

    class Meta:
        verbose_name_plural = 'Comments'


# Storing special situation related to the needy
@register_snippet
class SpecialSituation(models.Model):
    name = models.CharField(max_length=50)

    panels = [
        FieldPanel('name'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'special situations'

# Storing residence type, ex: tent, rented apartment  or owned

@register_snippet
class Charity(models.Model):
    charity_name = models.CharField(max_length=100, blank=False, null=True, verbose_name='charity name')
    charity_head = models.CharField(max_length=100, blank=False, null=True, verbose_name='charity head')
    charity_head_contact_info = models.CharField(max_length=100,
                                             blank=False, null=True, verbose_name='charity head contacts')
    town = models.ForeignKey('needy.Town',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name='Town name',
        related_name='+'
    )
    panels = [
        FieldPanel('charity_name'),
        FieldPanel('charity_head'),
        FieldPanel('charity_head_contact_info'),
        FieldPanel('town'),
    ]
    def __str__(self):
        return self.charity_name

    class Meta:
        verbose_name_plural = 'Charities'


class WalletAddressHistory(models.Model):
    """ Class for storing used wallets history"""
    address_owner = models.ForeignKey('needy.NeedyPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    #previous_wallet_address = None
    current_wallet_address = models.CharField(max_length=50)
    total_received = models.DecimalField(max_digits=33, decimal_places=8, blank=True, null=True)
    address_add_date = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(blank=True, null=True, verbose_name='active address')


    def __str__(self):
        return self.current_wallet_address

@register_snippet
class ResidenceType(models.Model):
    name = models.CharField(max_length=50)

    panels = [
        FieldPanel('name'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Residence Type'

@register_snippet
class Town(models.Model):
    name = models.CharField(max_length=50)

    panels = [
        FieldPanel('name'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Towns'

# NeedyPage._meta.get_field("title").verbose_name = "Title"
# NeedyPage._meta.get_field("banner_subtitle").verbose_name = "test"

class Meta:  # noqa
    verbose_name = "Needy Page"
    verbose_name_plural = "Needy Pages"

# set a default blank slug for when the editing form renders
# we set this after the model is declared

# disabled default slug field value and default title to avoid migrations issue
#NeedyPage._meta.get_field('slug').default = '11111'
#NeedyPage._meta.get_field("title").default = "New Default Title"
