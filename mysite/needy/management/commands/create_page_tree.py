from django.core.management.base import BaseCommand, CommandError
from wagtail.core.models import Page

from needy.models import NeedyIndexPage
from needy.models import SpecialSituation
from needy.models import ResidenceType

from home.models import HomePage

from django.core.management.base import BaseCommand

root_page = Page.objects.filter(pk=1).get()
home_page = Page.objects.filter(slug='home').get()
needy_index_page = NeedyIndexPage(title="Needy Index Page",intro="nothing here", slug="needyindexpage")


class Command(BaseCommand):
    """Create basic page structure"""
    
    def handle(self, *args, **options):
        home_page.add_child(instance=needy_index_page)

        SpecialSituation.objects.bulk_create([
            SpecialSituation(pk=1, name="Orphan"),
            SpecialSituation(pk=2, name="Widow"),
            SpecialSituation(pk=3, name="Disability"),
            ])
    
