from wagtail.core.signals import page_published
from needy.models import NeedyPage
from needy.models import WalletAddressHistory
from .address_total_received import get_token_total

# Specify the token ID to be monitored  on addresses
# in this example it's Spice token
tokenIdHex = "4de69e374a8ed21cbddd47f2338cc0f479dc58daa2bbe11cd604ca488eca0ddf"

# Do something clever for each model type
def receiver(sender, **kwargs):
    # Do something with blog posts
    instance = kwargs['instance']
    w = WalletAddressHistory()
    # Check if current wallet address is the same as the previous one
    # to start the function
    if instance.previous_wallet_address != instance.wallet_address:
        try:
            # Get the old active wallet address and set it to inactive
            old_active = WalletAddressHistory.objects.get(address_owner=instance, active=True)
            old_active.active = False
            old_active.total_received = get_token_total(tokenIdHex, old_active.current_wallet_address)
            old_active.save()
        except WalletAddressHistory.DoesNotExist:
            # If no old address like if new page, pass.
            pass
        # Store the new address in the addresses table
        w.current_wallet_address = instance.wallet_address
        # set the owner of the address to the page owner.
        w.address_owner = instance
        # Set the new address to active
        w.active = True
        # Create/modify previous wallet address in page
        instance.previous_wallet_address = instance.wallet_address
        instance.save()
        w.save()
        

# Register listeners for each page model class
page_published.connect(receiver, sender=NeedyPage)
