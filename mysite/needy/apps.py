from django.apps import AppConfig


class NeedyConfig(AppConfig):
    name = 'needy'

    def ready(self):
        import needy.signals  # noqa
