from django.templatetags.static import static
from django.utils.html import format_html

from wagtail.core import hooks
from wagtail.contrib.modeladmin.options import (
    ModelAdmin, modeladmin_register)
from .models import Comment


class CommentAdmin(ModelAdmin):
    model = Comment
    menu_label = 'Comments'  # ditch this to use verbose_name_plural from model
    menu_icon = 'pilcrow'  # change as required
    menu_order = 200  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = False # or True to exclude pages of this type from Wagtail's explorer view
    list_display = ('needy_review', 'needy_id_number')
    list_filter = ('needy_id_number',)
    search_fields = ('needy_review', 'needy_id_number')


# Now you just need to register your customised ModelAdmin class with Wagtail
modeladmin_register(CommentAdmin)



@hooks.register('construct_explorer_page_queryset')
# Show authors their articles only
def show_authors_only_their_articles(parent_page, pages, request):
    user_group = request.user.groups.filter(name='donee').exists()
    if user_group:
        pages = pages.filter(owner=request.user)
    return pages


@hooks.register('construct_image_chooser_queryset')
# Show images to the user that have uploaded them only in admin
def filter_images_by_user(images, request):
    images = images.filter(uploaded_by_user=request.user)

    return images

# Code to record audio in custom field hopefully
# @hooks.register("insert_editor_js", order=100)
# def global_admin_js():
#     """Add /static/css/custom.js to the admin."""
#     return format_html(
#         '<script src="{}"></script>',
#         static("/js/custom.js")
#     )
