# Needy

Needy people can submit their info and get direct donations using cryptocurrencies  

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. It's not complete yet.

### Prerequisites

What things you need to install the software and how to install them

```
Django 3.1
Wagtail 2.11.3
django-sesame 2.3
django-qr-code 2.1
```

### Install

```
# clone the repository
git clone git@gitlab.com:uak/needy.git
cd needy/
# Create the virtual environment and activate it
python3 -m venv vevn
source vevn/bin/activate
# Install requirements
cd mysite/
pip install -r requirements.txt
# Run migrations and set an admin password
./manage.py migrate
./manage.py createsuperuser
./manage.py runserver

```

## Built With

* [python3](https://www.python.org/) - The programming language used
* [django](https://www.djangoproject.com/) - The web framework used
* [wagtail](https://wagtail.io/) - The nice CMS that does hold them all

## Authors

* **Akad** 

## License

This project is licensed under the Affero General Public License.

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
